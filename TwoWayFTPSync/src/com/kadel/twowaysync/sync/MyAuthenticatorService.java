package com.kadel.twowaysync.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MyAuthenticatorService extends Service
{
	private MyAuthenticator mAuthenticator;
	
	@Override
	public void onCreate()
	{
		mAuthenticator = new MyAuthenticator(this);
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return mAuthenticator.getIBinder();
	}
}