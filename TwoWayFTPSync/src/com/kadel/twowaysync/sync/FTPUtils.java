package com.kadel.twowaysync.sync;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.os.Environment;
import android.util.Log;

import com.kadel.twowaysync.beans.SyncFileDetails;

@SuppressWarnings("unused")
public class FTPUtils {

	private FTPClient ftpClient;
	private static final String TAG = "FTPUtils";
	private List<SyncFileDetails> uploadFailed = new ArrayList<SyncFileDetails>();
	private List<SyncFileDetails> downloadFailed = new ArrayList<SyncFileDetails>();
	private List<SyncFileDetails> deleteFalied = new ArrayList<SyncFileDetails>();

	public FTPUtils(){
		ftpClient = new FTPClient();
	}

	public boolean connectAndLogin(String server, int port, String userId, String pass){
		boolean login = false;

		try{
			ftpClient.setConnectTimeout(1000 * 10);
			if(port == -1)
				ftpClient.connect((server));
			else
				ftpClient.connect((server), port);

			login = ftpClient.login(userId, pass);

			if(login == false){
				Log.e(TAG, "FTP client login failed");
				ftpClient.disconnect();
				return false;
			}
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.setKeepAlive(true);
			ftpClient.setSoTimeout(180 * 1000);
		}catch(IOException ex){
			Log.e(TAG, ex.getMessage());
			return false;
		}
		return login;
	}

	public void disconnectLogout(){
		if(ftpClient != null){
			try {
				ftpClient.logout();
				if(ftpClient.isConnected())
					ftpClient.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public FTPClient getFTPClient(){
		return ftpClient;
	}

	public boolean isConnected(){
		if(ftpClient != null)
			return ftpClient.isConnected();
		else
			return false;
	}

	public List<SyncFileDetails> uploadFiles(List<SyncFileDetails> local){
		uploadFailed.clear();
		ftpClient.enterLocalPassiveMode();

		for(int i = 0; i < local.size(); i++){
			SyncFileDetails file = local.get(i);

			String uploadPath = file.getSyncPath();
			InputStream fisLocal = null;
			/*
			 * check if server has folders
			 */
			try {
				String path = checkCreateFolder(uploadPath);

				fisLocal = new BufferedInputStream(new FileInputStream(file.getPath()));
				
				//now store the file on server..
				boolean uploaded = ftpClient.storeFile("/" + file.getSyncPath(), fisLocal);
				fisLocal.close();
				if(uploaded){
					/*
					 * update database after checksum
					 */
					InputStream fisRemote = ftpClient.retrieveFileStream(uploadPath);
					long fLocal = new File(file.getPath()).length();
					long fRemote = ftpClient.mlistFile(uploadPath).getSize();

					if(MD5CheckSum(fisRemote, fisLocal) == true && fLocal == fRemote){
						/*
						 * do nothing..
						 */
					}else{
						/*
						 * save these for future reference..
						 */
						uploadFailed.add(file);
						Log.e("FTP upload", "checksum error");
					}
				}else{
					uploadFailed.add(file);
					Log.e("FTP upload", "failed to upload");
				}
			} catch (IOException e) {
				uploadFailed.add(file);
				try{
					fisLocal.close();
				}catch(IOException ex){
					Log.e("FTP upload", e.toString());
				}
				Log.e("FTP upload", e.toString());
			}
		}
		return uploadFailed;
	}
	
	private String getSdcardPath(){
		String path = null;
		File root = new File("/mnt");
		File[] list = root.listFiles();
		for(int i = 0; i < list.length; i++){
			if(list[i].getName().equalsIgnoreCase("sdcard") || list[i].getName().equalsIgnoreCase("extsdcard")){
				File f = new File(list[i].getAbsolutePath());
				if(f.isDirectory() && f.exists() && f.canWrite() && f.isHidden() == false){
					path = f.getAbsolutePath();
					break;
				}
			}
		}
		if(path != null)
			return path;
		else
			return Environment.getExternalStorageDirectory().getAbsolutePath();
	}

	private void createDirs(String path, String name){
		String dirPath = path.split(name)[0];
		File dir = new File(dirPath);
		if(dir.exists() == false)
			dir.mkdirs();
	}
	
	public List<SyncFileDetails> downloadFiles(List<SyncFileDetails> remote){
		downloadFailed.clear();
		ftpClient.enterLocalPassiveMode();

		for(int i = 0; i < remote.size(); i++){
			SyncFileDetails file = remote.get(i);

			String uploadPath = file.getSyncPath();
			OutputStream out = null;
			/*
			 * check if server has folders
			 */
			try {
				createDirs(getSdcardPath() + "/" + uploadPath, file.getFileName());
				File dir = new File(getSdcardPath() + "/" + uploadPath);//instead change to default location
				
				InputStream fisRemote = ftpClient.retrieveFileStream(file.getPath());
				
				//now store the file on server..
				out = new BufferedOutputStream(new FileOutputStream(dir));
				boolean uploaded = ftpClient.retrieveFile("/" + uploadPath, out);
				out.close();
				ftpClient.getReplyCode();
				if(uploaded){
					/*
					 * update database after checksum
					 */
					FileInputStream fisLocal = new FileInputStream(file.getSyncPath());
					long fLocal = new File(file.getPath()).length();
					long fRemote = ftpClient.mlistFile(uploadPath).getSize();

					if(MD5CheckSum(fisRemote, fisLocal) == true && fLocal == fRemote){
						/*
						 * do nothing..
						 */
					}else{
						/*
						 * save these for future reference..
						 */
						downloadFailed.add(file);
						Log.e("FTP upload", "checksum error");
					}
				}else{
					downloadFailed.add(file);
					Log.e("FTP upload", "failed to upload");
				}
			} catch (IOException e) {
				downloadFailed.add(file);
				try{
					if(out != null)
						out.close();
				}catch(IOException ex){
					Log.e("FTP upload", e.toString());
				}
				Log.e("FTP upload", e.toString());
			}
		}
		return downloadFailed;
	}

	private String checkCreateFolder(String uploadPath){
		String path1 = "";
		try{
			ftpClient.changeWorkingDirectory("/");//instead change to default location

			String checkPaths[] = uploadPath.split("/");
			for(int j = 0; j < checkPaths.length - 1; j++){
				if(checkPaths[j].equals("") == false){
					path1 = path1 + "/" + checkPaths[j];
					ftpClient.changeWorkingDirectory(checkPaths[j]);

					if(ftpClient.getReplyCode() == 550){
						ftpClient.makeDirectory(checkPaths[j]);
					}
				}
			}
		}catch (IOException e) {
			Log.e("FTP create folder", e.getMessage());
		}
		return path1;
	}

	public List<SyncFileDetails> checkIfExistsOnServer(List<SyncFileDetails> files){
		List<SyncFileDetails> missing = new ArrayList<SyncFileDetails>();

		for(int i = 0; i < files.size(); i++){
			try{
				ftpClient.changeToParentDirectory();

				String uploadPath = files.get(i).getSyncPath();
				InputStream io = ftpClient.retrieveFileStream(uploadPath);
				if(io == null || ftpClient.getReplyCode() == 550){
					missing.add(files.get(i));
				}
			}catch(IOException e){
				Log.e("FTP Check missing files", e.getMessage());
			}
		}
		return missing;
	}

	public List<SyncFileDetails> checkForOutdated(List<SyncFileDetails> files){
		List<SyncFileDetails> outdated = new ArrayList<SyncFileDetails>();

		try{
			for(int i = 0; i < files.size(); i++){
				ftpClient.changeToParentDirectory();

				String uploadPath = files.get(i).getSyncPath();
				InputStream io = ftpClient.retrieveFileStream(uploadPath);

				if(io == null || ftpClient.getReplyCode() == 550){
					//file is missing..
				}else{
					File fileLocal = new File(files.get(i).getPath());
					FTPFile fileRemote = ftpClient.mlistFile(uploadPath);

					long local = fileLocal.lastModified();
					long remote = fileRemote.getTimestamp().getTimeInMillis();

					if(local > remote || fileLocal.length() > fileRemote.getSize()){
						/*
						 * if time is past or size is greater than
						 * add to list..
						 */
						outdated.add(files.get(i));
					}
				}
			}
		}catch(IOException e){
			Log.e("FTP outdated on server", e.getMessage());
		}
		return outdated;
	}

	public void deleteFiles(List<SyncFileDetails> delete){
		for(int i = 0; i < delete.size(); i++){
			try {
				if(ftpClient.deleteFile(delete.get(i).getPath()) == false)
					deleteFalied.add(delete.get(i));
			} catch (IOException e) {
				Log.e("FTP file delete", e.getMessage());
			}
		}
	}

	private boolean MD5CheckSum(InputStream remote, InputStream local){

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}

		byte[] dataBytes = new byte[1024];

		int nread = 0; 
		try {
			while ((nread = remote.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		byte[] mdbytes = md.digest();

		StringBuffer remoteString = new StringBuffer();
		for (int i = 0; i < mdbytes.length; i++) {
			String hex = Integer.toHexString(0xff & mdbytes[i]);
			if(hex.length() == 1) remoteString.append('0');
			remoteString.append(hex);
		}

		md.reset();
		nread = 0;
		try {
			while ((nread = local.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		mdbytes = null;
		mdbytes = new byte[1024];
		mdbytes = md.digest();

		StringBuffer localString = new StringBuffer();
		for (int i = 0; i < mdbytes.length; i++) {
			String hex = Integer.toHexString(0xff & mdbytes[i]);
			if(hex.length() == 1) localString.append('0');
			localString.append(hex);
		}

		if(remoteString.equals(localString)){
			return true;
		}else
			return false;
	}
}