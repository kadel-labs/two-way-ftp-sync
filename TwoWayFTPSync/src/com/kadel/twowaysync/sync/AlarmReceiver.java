package com.kadel.twowaysync.sync;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.db.DataManager;
import com.kadel.twowaysync.utils.Constants;
import com.kadel.twowaysync.utils.Utilities;

public class AlarmReceiver extends BroadcastReceiver{

	private DataManager dataManager;
	private Utilities utils;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		android.os.Debug.waitForDebugger();
		
		dataManager = new DataManager();
		utils = new Utilities();
		
		dataManager.setWritableState();
		FTPCredentials cred = dataManager.getFTPCredentials(intent.getAction()).get(0);
		cred.setSyncStatus("In queue");
		ContentValues values = utils.createFTPCredentials(cred);
		dataManager.updateDatabase(Constants.FTP_CRED_TABLE, values, intent.getAction(), Constants.FTP_NAME , null, null);
		dataManager.close();
		String account = intent.getAction();
		int id = intent.getIntExtra("id", 0);
		long interval = intent.getLongExtra("interval", 1000);
		
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent service = new Intent(context, FTPSyncService.class);
		service.setAction(account);
		
		PendingIntent pIntent = PendingIntent.getService(context, id, service, PendingIntent.FLAG_UPDATE_CURRENT);
		manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, interval, pIntent);
	}

}
