package com.kadel.twowaysync.sync;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.beans.FileMetaDeta;
import com.kadel.twowaysync.beans.SyncFileDetails;
import com.kadel.twowaysync.db.DataManager;
import com.kadel.twowaysync.utils.Constants;
import com.kadel.twowaysync.utils.Utilities;

@SuppressWarnings("unused")
public class FTPSyncService extends IntentService{

	private DataManager dataManager;
	private FTPUtils ftp;
	private String server;
	private int port;
	private String userName;
	private String password;
	private String localSyncPath, remoteSyncPath;
	private Utilities utils;
	private ContentValues val;
	private FTPCredentials cred;
	
	public FTPSyncService() {
		super("FTP Sync Service");
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if(utils == null)
			utils = new Utilities();
		if(ftp == null)
			ftp = new FTPUtils();

		String account = intent.getAction();

		if(dataManager == null)
			dataManager = new DataManager();
		dataManager.setWritableState();

		cred = dataManager.getFTPCredentials(account).get(0);
		server = cred.getServer();
		port = cred.getPort();
		userName = cred.getUserName();
		password = cred.getPassword();

		cred.setSyncStatus("Syncing");
		val = utils.createFTPCredentials(cred);
		dataManager.updateDatabase(Constants.FTP_CRED_TABLE, val, account, Constants.FTP_NAME, null, null);
		
		List<FileMetaDeta> l = dataManager.getFTPLocalSync(account);
		List<FileMetaDeta> r = dataManager.getFTPRemoteSync(account);

		ftp.connectAndLogin(server, port, userName, password);

		List<SyncFileDetails> local = new ArrayList<SyncFileDetails>();
		List<SyncFileDetails> remote = new ArrayList<SyncFileDetails>();

		for(int i = 0; i < l.size(); i++){
			local.addAll(utils.getLocalFiles(cred.getLocalSyncScheme(), l.get(i).getSyncPath(), l.get(i).getSyncPath()));
		}
		for(int i = 0; i < r.size(); i++){
			remote.addAll(utils.getRemoteFiles(cred.getRemoteSyncScheme(), r.get(i).getSyncPath(), r.get(i).getSyncPath(), ftp.getFTPClient()));
		}

		Date localSyncTime = null, remoteSyncTime = null, syncTime = null;

		if(local.isEmpty() == false)
			localSyncTime = doLocalSync(local, cred, l);
		if(remote.isEmpty() == false)
			remoteSyncTime = doRemoteSync(remote, cred, r);

		if(remoteSyncTime != null && localSyncTime != null && remoteSyncTime.after(localSyncTime))
			syncTime = remoteSyncTime;
		else if(remoteSyncTime != null && localSyncTime != null && localSyncTime.after(remoteSyncTime))
			syncTime = localSyncTime;

		cred.setSyncStatus("complete");
		if(syncTime != null)
			cred.setLastSync(syncTime.toString());
		
		val.clear();
		val = utils.createFTPCredentials(cred);

		dataManager.updateDatabase(Constants.FTP_CRED_TABLE, val, account, Constants.FTP_NAME, null, null);
		val.clear();
	}

	private Date doLocalSync(List<SyncFileDetails> local, FTPCredentials cred, List<FileMetaDeta> l){
		int wifiMode = cred.getSyncOnWifi();
		String lastSync = cred.getLastSync();

		if(checkForConnectivity(wifiMode) == true){
			if(ftp == null)
				ftp = new FTPUtils();
			boolean login = false;
			if(ftp.isConnected() == false)
				login = ftp.connectAndLogin(server, port, userName, password);
			else
				login = true;
				
			if(login){
				if(lastSync == null){
					/*
					 * upload all files..
					 */
					ftp.uploadFiles(local);
				}else{
					/*
					 * check for missing/outdated files 
					 * on server first..
					 */
					List<SyncFileDetails> remote = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> uploadFiles = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> deletedLocal = new ArrayList<SyncFileDetails>();

					List<SyncFileDetails> downloadFiles = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> deletedRemote = new ArrayList<SyncFileDetails>();

					//get missing files..
					uploadFiles.addAll(ftp.checkIfExistsOnServer(local));
					//get the outdated files on server..
					uploadFiles.addAll(ftp.checkForOutdated(local));

					for(int i = 0; i < l.size(); i++){
						File f = new File(l.get(i).getSyncPath());
						String[] s = l.get(i).getSyncPath().split("/");

						String path = "/" + s[s.length-1];
						remote.addAll(utils.getRemoteFiles(0, "/" + s[s.length-1], "/" + s[s.length-1], ftp.getFTPClient()));//give default if exists
					}

					//get deleted files on local..
					long localTime = getLatestModificationTimeLocal(local);
					long remoteTime = getLatestModificationTimeRemote(remote, ftp.getFTPClient());

					for(int i = 0; i < remote.size(); i++){
						String remoteFile = remote.get(i).getSyncPath();//here sync path actual path

						boolean notDeleted = false;
						for(int j = 0; j < local.size(); j++){
							if(local.get(j).getPath().equals(remoteFile)){
								notDeleted = true;
								break;
							}
						}
						if(notDeleted == false)
							deletedLocal.add(remote.get(i));
					}

					//if both way sync then only
					if(cred.getLocalSyncMode() == 1){
						//get missing files on local..
						//here deleted local are considered as missing on device, now we need to check
						//which modification time is greater..
						if(localTime > remoteTime){
							//this means local was updated latest..so upload and delete from server..
							ftp.uploadFiles(uploadFiles);
							ftp.deleteFiles(deletedLocal);
						}else{
							//this means server is modified latest..so deleted files are actually missing files..
							//add these to download..
							downloadFiles.addAll(deletedLocal);//missing files on local..

							//check for outdated files now..
							for(int i = 0; i < remote.size(); i++){
								String remoteFile = remote.get(i).getSyncPath();//here sync path actual path

								FTPClient ftpClient = ftp.getFTPClient();
								for(int j = 0; j < local.size(); j++){
									if(local.get(j).getPath().equals(remoteFile)){
										//if file is present, check for date..
										File fLocal = new File(local.get(j).getPath());
										long lTime = fLocal.lastModified();
										long rTime = 0;
										try {
											rTime = ftpClient.mlistFile(remoteFile).getTimestamp().getTimeInMillis();
										} catch (IOException e) {
											e.printStackTrace();
										}
										if(rTime > lTime)
											downloadFiles.add(remote.get(j));//outdated files..
									}
								}
							}

							//now deleted on remote..
							for(int i = 0; i < remote.size(); i++){
								String remoteFile = remote.get(i).getSyncPath();//here sync path actual path

								boolean deleted = true;
								for(int j = 0; j < local.size(); j++){
									if(local.get(j).getPath().equals(remoteFile)){
										deleted = false;
										break;
									}
								}
								if(deleted == true)
									deletedRemote.add(local.get(i));
							}
							ftp.uploadFiles(uploadFiles);
							ftp.downloadFiles(downloadFiles);
							deleteFiles(deletedRemote);
						}
					}else{
						ftp.uploadFiles(uploadFiles);
						ftp.deleteFiles(deletedLocal);
					}
				}
			}else{
				Log.e("FTP local sync", "login failed");
				return null;
			}
			/*
			 * close the ftp connection
			 */
			ftp.disconnectLogout();
			return new Date(System.currentTimeMillis());
		}else{
			Log.d("FTPSyncService", "No internet connection");
			return null;
		}
	}

	private Date doRemoteSync(List<SyncFileDetails> remote, FTPCredentials cred, List<FileMetaDeta> r){
		int wifiMode = cred.getSyncOnWifi();

		if(checkForConnectivity(wifiMode) == true){
			if(ftp == null)
				ftp = new FTPUtils();
			boolean login = false;
			
			if(ftp.isConnected() == false)
				login = ftp.connectAndLogin(server, port, userName, password);
			else
				login = true;
			
			if(login == true){
				if(cred.getLastSync() == null){
					/*
					 * download all the files..
					 */
					ftp.downloadFiles(remote);
				}else{
					/*
					 * start checking for updates.. 
					 */
					List<SyncFileDetails> local = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> download = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> deletedRemote = new ArrayList<SyncFileDetails>();
					
					List<SyncFileDetails> upload = new ArrayList<SyncFileDetails>();
					List<SyncFileDetails> deletedLocal = new ArrayList<SyncFileDetails>();
					
					FTPClient ftpClient = ftp.getFTPClient();
					for(int i = 0; i < r.size(); i++){
						String[] s = r.get(i).getSyncPath().split("/");

						FTPFile f = null;
						try {
							f = ftpClient.mlistFile(r.get(i).getSyncPath());
						} catch (IOException e) {
							e.printStackTrace();
						}
						String path = Environment.getExternalStorageDirectory().getAbsolutePath() + s[s.length-1];
						
						if(f != null && f.getName().startsWith(".") == false)
							local.addAll(utils.getLocalFiles(0, path, path));
					}
						
					long remoteTime = getLatestModificationTimeRemote(remote, ftpClient);
					long localTime = getLatestModificationTimeLocal(local);
					
					//get missing/outdated on local..
					for(int i = 0; i < remote.size(); i++){
						String path = remote.get(i).getSyncPath();
						
						for(int  j = 0; j < local.size(); j++){
							String localPath = local.get(j).getPath().
									split(Environment.getExternalStorageDirectory().getAbsolutePath())[1];
							
							if(localPath.equals(path)){
								//this means file is present on local..
								File lFile = new File(local.get(j).getPath());
								FTPFile rFile = null;
								long lTime = lFile.lastModified();
								long rTime = 0;
								
								try {
									rFile = ftpClient.mlistFile(remote.get(i).getPath());
								} catch (IOException e) {
									e.printStackTrace();
								}
								rTime = rFile.getTimestamp().getTimeInMillis();
								
								if(rTime > lTime || lFile.length() < rFile.getSize())
									download.add(remote.get(i));
								break;
							}else
								download.add(remote.get(i));
						}
					}
					
					for(int i = 0; i < local.size(); i++){
						String syncPath = local.get(i).getSyncPath();
						try{
							InputStream io = ftpClient.retrieveFileStream(syncPath);
							if(io == null && ftpClient.getReplyCode() == 505){
								//this means not present on remote..
								upload.add(local.get(i));
							}
						}catch(IOException e){
							Log.e("FTP remote upload", e.getMessage());
						}
					}
					
					if(cred.getRemoteSyncMode() == 1){
						//this is both way sync..
						ftp.downloadFiles(download);
						ftp.uploadFiles(upload);
						if(remoteTime > localTime){
							//this is as good as one way sync for delete..
							deleteFiles(deletedLocal);
						}else{
							//this is opposite if above..
							ftp.deleteFiles(deletedRemote);
						}
					}else{
						ftp.downloadFiles(download);
						deleteFiles(deletedLocal);
					}
				}
			}else{
				Log.e("FTP remote Sync", "Login failed");
				return null;
			}
			/*
			 * close the ftp connection..
			 */
			ftp.disconnectLogout();
			return new Date(System.currentTimeMillis());
		}else{
			Log.d("FTPSyncService", "No internet connection");
			return null;
		}
	}

	/**
	 * delete files on local
	 */
	private List<SyncFileDetails> deleteFiles(List<SyncFileDetails> local){
		List<SyncFileDetails> deleteFailed = new ArrayList<SyncFileDetails>();

		for(int i = 0; i < local.size(); i++){
			String path = local.get(i).getPath();
			File localFile = new File(path);

			if(localFile.exists()){
				if(localFile.delete() == false)
					deleteFailed.add(local.get(i));
			}
		}
		return deleteFailed;
	}

	/**
	 * depending on wifiMode, method returns
	 * true or false
	 * 
	 */
	private boolean checkForConnectivity(int wifiMode){
		ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetwork = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileData = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		boolean connected = false;
		if(wifiMode == 1){
			if(wifiNetwork == null){
				/*
				 * no wifi on device..
				 */
				return false;
			}else{
				return getState(wifiNetwork);
			}
		}else{
			if(wifiNetwork != null){
				connected = getState(wifiNetwork);
			}
			if(connected == false){
				if(mobileData != null){
					connected = getState(mobileData);
				}
			}
		}
		return connected;
	}

	private boolean getState(NetworkInfo network){
		if(network.getState() == android.net.NetworkInfo.State.CONNECTED){
			return true;
		}else
			return false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	private long getLatestModificationTimeRemote(List<SyncFileDetails> remote, FTPClient ftpClient){
		long time = 0L;
		for(int i = 0; i < remote.size(); i++){
			FTPFile file = null;
			try{
				file = ftpClient.mlistFile(remote.get(i).getPath());
			}catch(IOException e){
				Log.e("FTP file time", e.getMessage());
			}
			if(file.getTimestamp().getTimeInMillis() > time)
				time = file.getTimestamp().getTimeInMillis();
		}
		return time;
	}

	private long getLatestModificationTimeLocal(List<SyncFileDetails> local){
		long time = 0L;
		for(int i = 0; i < local.size(); i++){
			File f = new File(local.get(i).getPath());
			if(f.lastModified() > time)
				time = f.lastModified();
		}
		return time;
	}
}
