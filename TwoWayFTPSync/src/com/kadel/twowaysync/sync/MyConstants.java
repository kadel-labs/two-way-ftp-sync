package com.kadel.twowaysync.sync;

import android.net.Uri;

public class MyConstants
{
	public static final String AUTHORITY = "com.kadel.syncapplication.fileMeta.provider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/file_meta_data");
}
