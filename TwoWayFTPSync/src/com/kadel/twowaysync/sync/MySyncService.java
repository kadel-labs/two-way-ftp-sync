package com.kadel.twowaysync.sync;

import java.io.File;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class MySyncService extends Service {
	private static MySyncAdaptor sSyncAdapter = null;
	public static File FILE_DIRECTORY;
	private static final Object sSyncAdapterLock = new Object();
//	private

	@Override
	public void onCreate() {
		Toast.makeText(getApplicationContext(), "Starting sync service..", Toast.LENGTH_SHORT).show();
		synchronized (sSyncAdapterLock) {
			FILE_DIRECTORY = getFilesDir();
			if (sSyncAdapter == null)
				sSyncAdapter = new MySyncAdaptor(getApplicationContext(), true);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return sSyncAdapter.getSyncAdapterBinder();
	}
}