package com.kadel.twowaysync.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.kadel.twowayftpsync.R;
import com.kadel.twowaysync.adapter.FTPListAdapter;
import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.db.DataManager;
import com.kadel.twowaysync.utils.Constants;

public class ListAccount extends Activity{

	private ListView list;
	private FTPListAdapter adapter;
	private DataManager dataManager;
	private List<FTPCredentials> items;
	private LinearLayout layout;
	private int position;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ftplist_view);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		list = (ListView) findViewById(R.id.account_list);
		layout = (LinearLayout) findViewById(R.id.no_accounts);
		
		dataManager = new DataManager();
		dataManager.setWritableState();
		items = dataManager.getFTPCredentials(null);
		
		adapter = new FTPListAdapter(this, R.layout.account_list_item, items);
		list.setAdapter(adapter);
		
		if(items == null || items.isEmpty()){
			layout.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ListAccount.this.position = position;
				Toast.makeText(ListAccount.this, ListAccount.this.position + " position clicked", Toast.LENGTH_SHORT).show();
			}
		});
		
		/*
		 * register listener
		 * for list item long press
		 */
		list.setOnItemLongClickListener(new ListView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
				position = pos;
				startActionMode(callback);
				view.setSelected(true);
				return true;
			}
		});
	}
	
	private ActionMode.Callback callback = new ActionMode.Callback(){
		
		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			return false;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;
		}
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("options");
			mode.getMenuInflater().inflate(R.menu.list_account_menu, menu);
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			
			int id = item.getItemId();
			switch (id) {
			case R.id.delete_account:{
				FTPCredentials cred = adapter.getItem(position);
				dataManager.deleteContent(Constants.FTP_CRED_TABLE, Constants.FTP_NAME, cred.getAccountName());
				dataManager.deleteContent(Constants.FTP_LOCAL_PATHS, Constants.SYNC_ACCOUNT_NAME, cred.getAccountName());
				dataManager.deleteContent(Constants.FTP_REMOTE_PATHS, Constants.SYNC_ACCOUNT_NAME, cred.getAccountName());
				adapter.remove(cred);
				adapter.notifyDataSetChanged();
				mode.finish();
				break;
			}

			case R.id.add_account_menu:{
				Intent add = new Intent(ListAccount.this, Main.class);
				startActivity(add);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
				mode.finish();
				ListAccount.this.finish();
				break;
			}
			
			case R.id.edit_account:{
				Toast.makeText(ListAccount.this, "no actions", Toast.LENGTH_SHORT).show();
				mode.finish();
				break;
			}
			}
			return false;
		}
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if(id == android.R.id.home){
			NavUtils.navigateUpFromSameTask(this);
			overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
	}
}