package com.kadel.twowaysync.activity;

import com.kadel.twowaysync.db.DataManager;

import android.app.Application;

public class TwoWayFTPSyncApp extends Application{

	/**
	 * giving a static context to DataManager
	 * on create of application
	 */
	public void onCreate() {
		super.onCreate();
		new DataManager(this);
	};
}
