package com.kadel.twowaysync.activity;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.InputType;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.kadel.twowayftpsync.R;
import com.kadel.twowaysync.adapter.FileExplorerAdapter;
import com.kadel.twowaysync.beans.CheckBoxState;
import com.kadel.twowaysync.beans.ExplorerItem;
import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.beans.SyncFileDetails;
import com.kadel.twowaysync.db.DataManager;
import com.kadel.twowaysync.sync.AlarmReceiver;
import com.kadel.twowaysync.sync.FTPUtils;
import com.kadel.twowaysync.utils.Constants;
import com.kadel.twowaysync.utils.Utilities;

public class Main extends Activity {

	private Button remoteBrowse, localBrowse, selectFol, createFol, selectAll, save;
	private EditText accountName, serverAdd, serverPort, serverUserId, serverPass;
	private ImageView undo;
	private ImageView nameDone, serverDone, portDone, userDone, passDone, localBrowseDone, remoteBrowseDone;
	private Spinner syncFreq, syncModeLocal, syncModeServer, syncSchemeLocal, syncSchemeServer;
	private CheckBox wifiOnly, combineMode, checkSum, showNotification;
	private ArrayAdapter<CharSequence> syncFreqAdapter;
	private ArrayAdapter<CharSequence> localSyncAdapter, remoteSyncAdapter, syncSchemeLocalAdapter, syncSchemeRemoteAdapter;
	private Context context;
	private FileExplorerAdapter localAdapter, remoteAdapter;

	private static final String localRoot = "/mnt";
	private ListView localList, remoteList;
	private LinearLayout progress;
	private TextView title;

	private int sync_freq = 30;
	private int sync_wifi_only = 1;
	private int sync_mode_local = 0, sync_mode_server = 0, sync_scheme_local = 1, sync_scheme_server = 1;
	private static boolean enableCheck = true;
	private static boolean showNotes = true;
	private List<String> localSync = new ArrayList<String>();
	private List<String> remoteSync = new ArrayList<String>();
	private List<SyncFileDetails> localFiles = new ArrayList<SyncFileDetails>();
	private List<SyncFileDetails> remoteFiles = new ArrayList<SyncFileDetails>();
	private DataManager dataManager;
	private RemoteExplorer remoteExplorer;
	private SaveToDatabase saveDatabase;
	private FTPClient ftpClient;
	private FTPUtils ftpUtils;
	private static final String DIVIDER = "/";
	private String currentDir = "";
	private long id;
	private ProgressDialog saving;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		context = Main.this;
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		accountName = (EditText) findViewById(R.id.ftp_account_name);
		nameDone = (ImageView) findViewById(R.id.name_done);

		localBrowse = (Button) findViewById(R.id.local_browser);
		localBrowseDone = (ImageView) findViewById(R.id.local_browse_done);

		remoteBrowse = (Button) findViewById(R.id.remote_browser);
		remoteBrowseDone = (ImageView) findViewById(R.id.remote_browse_done);

		serverAdd = (EditText) findViewById(R.id.ftp_server);
		serverDone = (ImageView) findViewById(R.id.server_done);

		serverPort = (EditText) findViewById(R.id.ftp_port);
		portDone = (ImageView) findViewById(R.id.port_done);

		serverUserId = (EditText) findViewById(R.id.ftp_username);
		userDone = (ImageView) findViewById(R.id.username_done);

		serverPass = (EditText) findViewById(R.id.ftp_pass);
		passDone = (ImageView) findViewById(R.id.password_done);

		syncFreq = (Spinner) findViewById(R.id.sync_freq);

		syncModeLocal = (Spinner) findViewById(R.id.sync_mode_local);

		syncModeServer = (Spinner) findViewById(R.id.sync_mode_server);

		syncSchemeLocal = (Spinner) findViewById(R.id.sync_scheme_local);

		syncSchemeServer = (Spinner) findViewById(R.id.sync_scheme_remote);

		save = (Button) findViewById(R.id.save_exit);

		wifiOnly = (CheckBox) findViewById(R.id.wifi_only);
		if(sync_wifi_only == 1)
			wifiOnly.setChecked(true);
		else
			wifiOnly.setChecked(false);

		combineMode = (CheckBox) findViewById(R.id.wifi_mobile);
		if(sync_wifi_only == 1)
			combineMode.setChecked(false);
		else
			combineMode.setChecked(true);

		checkSum = (CheckBox) findViewById(R.id.checksum);
		checkSum.setChecked(enableCheck);

		showNotification = (CheckBox) findViewById(R.id.notifications);
		showNotification.setChecked(showNotes);

		syncFreqAdapter = ArrayAdapter.createFromResource(this, R.array.sync_frequency, android.R.layout.simple_list_item_1);
		syncFreq.setAdapter(syncFreqAdapter);
		syncFreq.setSelection(0);

		localSyncAdapter = ArrayAdapter.createFromResource(this, R.array.sync_mode_local, android.R.layout.simple_list_item_1);
		syncModeLocal.setAdapter(localSyncAdapter);
		syncModeLocal.setSelection(0);

		remoteSyncAdapter = ArrayAdapter.createFromResource(context, R.array.sync_mode_server, android.R.layout.simple_list_item_1);
		syncModeServer.setAdapter(remoteSyncAdapter);
		syncModeServer.setSelection(0);

		syncSchemeLocalAdapter = ArrayAdapter.createFromResource(context, R.array.sync_scheme, android.R.layout.simple_list_item_1);
		syncSchemeLocal.setAdapter(syncSchemeLocalAdapter);
		syncSchemeLocal.setSelection(sync_scheme_local);

		syncSchemeRemoteAdapter = ArrayAdapter.createFromResource(context, R.array.sync_scheme, android.R.layout.simple_list_item_1);
		syncSchemeServer.setAdapter(syncSchemeRemoteAdapter);
		syncSchemeServer.setSelection(sync_scheme_server);


		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		localBrowse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showLocalExplorer();
			}
		});

		remoteBrowse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showRemoteExplorer();
			}
		});

		accountName.setOnFocusChangeListener(new FocusListener());
		serverAdd.setOnFocusChangeListener(new FocusListener());
		serverPort.setOnFocusChangeListener(new FocusListener());
		serverUserId.setOnFocusChangeListener(new FocusListener());
		serverPass.setOnFocusChangeListener(new FocusListener());

		serverPass.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					String text = serverPass.getText().toString();
					if (text != null && !text.equals("") && !text.equals(" ")) {
						passDone.setVisibility(View.VISIBLE);
					} else
						passDone.setVisibility(View.INVISIBLE);
				}
				return false;
			}
		});

		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * first save the credentials
				 * in db..
				 */
				FTPCredentials cred = new FTPCredentials();
				cred.setAccountName(accountName.getText().toString().trim());
				cred.setServer(serverAdd.getText().toString().trim());
				int port = 0;
				if(serverPort.getText().toString().trim() != null && !serverPort.getText().toString().trim().equals(""))
					port = Integer.parseInt(serverPort.getText().toString());
				else
					port = -1;

				cred.setPort(port);
				cred.setUserName(serverUserId.getText().toString().trim());
				cred.setPassword(serverPass.getText().toString().trim());
				cred.setSyncInterval(sync_freq * 1000 * 60);
				cred.setSyncOnWifi(sync_wifi_only);
				cred.setLocalSyncScheme(sync_scheme_local);
				cred.setLocalSyncMode(sync_mode_local);
				cred.setRemoteSyncScheme(sync_scheme_server);
				cred.setRemoteSyncMode(sync_mode_server);
				cred.setSyncStatus("inactive");
				cred.setLastSync(null);

				if(saving != null && saving.isShowing())
					saving.dismiss();
				saveDatabase = new SaveToDatabase(cred);
				saveDatabase.execute();

				//				PendingIntent pIntent = PendingIntent.getBroadcast(context, (int)id, sync, PendingIntent.FLAG_UPDATE_CURRENT);

				/*AlarmManager mgrAlarm = (AlarmManager) context.getSystemService(ALARM_SERVICE);
				ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();
				String[] action = {"Alarm"};

				for (int i = 0; i < 10; i++) {
					intent.setAction(action[0] + i);
					// Loop counter `i` is used as a `requestCode`
					PendingIntent pendingIntent = PendingIntent.getBroadcast(context, i, intent, PendingIntent.FLAG_ONE_SHOT);
					// Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
					mgrAlarm.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, i, pendingIntent);

					intentArray.add(pendingIntent);
				}*/
			}
		});

		wifiOnly.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				buttonView.setChecked(isChecked);
				if(isChecked)
					sync_wifi_only = 1;
				else
					sync_wifi_only = 0;
				combineMode.setChecked(!isChecked);
			}
		});

		combineMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				buttonView.setChecked(isChecked);
				if(isChecked)
					sync_wifi_only = 0;
				else
					sync_wifi_only = 1;
				wifiOnly.setChecked(!isChecked);
			}
		});

		syncFreq.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sync_freq = Integer.parseInt(parent.getItemAtPosition(position).toString());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing..
			}
		});

		syncModeLocal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sync_mode_local = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing..
			}
		});

		syncModeServer.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sync_mode_server = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing..
			}
		});

		syncSchemeLocal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sync_scheme_local = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing..
			}
		});

		syncSchemeServer.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sync_scheme_server = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//do nothing..
			}
		});
	}

	private void showLocalExplorer() {
		final Dialog explorer = new Dialog(context);
		explorer.setTitle("Explorer");
		explorer.requestWindowFeature(Window.FEATURE_NO_TITLE);
		explorer.setContentView(R.layout.local_explorer);
		explorer.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		explorer.setCanceledOnTouchOutside(false);

		LayoutParams params = explorer.getWindow().getAttributes();

		Display display = getWindowManager().getDefaultDisplay();
		Point p = new Point();
		display.getSize(p);
		int defHeight = p.y;

		params.height = (int) (0.70 * defHeight);

		localList = (ListView) explorer.findViewById(R.id.list_explorer);
		title = (TextView) explorer.findViewById(R.id.path);
		undo = (ImageView) explorer.findViewById(R.id.undo);
		selectFol = (Button) explorer.findViewById(R.id.select);
		createFol = (Button) explorer.findViewById(R.id.create);
		selectAll = (Button) explorer.findViewById(R.id.select_all);

		File rootDir = new File(localRoot);
		fill(rootDir);

		selectAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				localAdapter.setCheckBoxVisibilty(true);
				int count = localAdapter.getCount();
				for(int i = 0; i < count; i++){
					localAdapter.setChecked(i, true);
				}
				localAdapter.notifyDataSetChanged();
				undo.setVisibility(View.VISIBLE);
			}
		});

		selectFol.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * get and 
				 * store the paths
				 * to sync..
				 */
				localSync.clear();
				List<CheckBoxState> checked = localAdapter.getCheckedItems();

				for(int i = 0; i < checked.size(); i++){
					CheckBoxState item = checked.get(i);

					if(item.isChecked()){
						if(localSync.contains(item.getItem().getPath()) == false)
							localSync.add(item.getItem().getPath());
					}
				}
				explorer.dismiss();
				if(localSync.isEmpty() == false){
					localBrowseDone.setVisibility(View.VISIBLE);
					localBrowse.setText("Selected");
				}
			}
		});

		createFol.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String path = title.getText().toString();

				AlertDialog.Builder folder = new AlertDialog.Builder(context);
				folder.setTitle("Create Folder");
				LinearLayout layout = new LinearLayout(context);
				layout.setOrientation(LinearLayout.HORIZONTAL);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.setMargins(10, 10, 10, 10);

				layout.setLayoutParams(params);

				TextView msg = new TextView(context);
				msg.setText("Folder name: ");
				layout.addView(msg);

				final EditText folderName = new EditText(context);
				params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f);
				folderName.setLayoutParams(params);
				folderName.setInputType(InputType.TYPE_CLASS_TEXT);
				layout.addView(folderName);

				folder.setView(layout);

				folder.setCancelable(false);
				folder.setNegativeButton("Cancel", null);

				folder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String folder = folderName.getText().toString();
						File checkPath = new File(path);
						boolean canCreate = checkPath.canWrite();

						if(canCreate == false){
							Toast.makeText(context, "can not create folder, access denied", Toast.LENGTH_SHORT).show();
						}else{
							String folderPath = path + "/" + folder;
							File create = new File(folderPath);

							if(create.exists()){
								Toast.makeText(context, "folder exists", Toast.LENGTH_SHORT).show();
							}else if(create.mkdir()){
								Toast.makeText(context, "folder created", Toast.LENGTH_SHORT).show();
								fill(new File(title.getText().toString()));
							}
						}
					}
				});
				folder.show();
			}
		});

		localList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				boolean isCheckBoxVisible = localAdapter.getCheckBoxVisibilty();
				ExplorerItem item = localAdapter.getItem(position);
				if (isCheckBoxVisible) {

					if (item.getImage().equalsIgnoreCase("folder") || item.getImage().equalsIgnoreCase("file")) {
						localAdapter.setChecked(position, !localAdapter.getChecked(position));
						localAdapter.notifyDataSetChanged();
					} else {
						File currentDir = new File(item.getPath());
						fill(currentDir);
					}
				} else {
					if (item.getImage().equalsIgnoreCase("folder")
							|| item.getImage().equalsIgnoreCase("direc_up")
							|| item.getImage().equalsIgnoreCase("internal")
							|| item.getImage().equalsIgnoreCase("memory_card")) {
						File currentDir = new File(item.getPath());
						fill(currentDir);
					} else {
						onFileClick(item);
					}
				}
			}
		});

		localList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				ExplorerItem item = localAdapter.getItem(position);

				if (item.getImage().equalsIgnoreCase("direc_up")) {
					return true;
				} else {
					if (localAdapter.getCheckBoxVisibilty() == true)
						return true;

					localAdapter.setChecked(position, true);
					localAdapter.setCheckBoxVisibilty(true);
					localAdapter.notifyDataSetChanged();
					undo.setVisibility(View.VISIBLE);
				}
				return true;
			}
		});

		undo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(localAdapter.getCheckBoxVisibilty()){
					int count = localAdapter.getCount();
					for(int i = 0; i < count; i++){
						localAdapter.setChecked(i, false);
					}
					localAdapter.setCheckBoxVisibilty(false);
					localAdapter.notifyDataSetChanged();
				}
				undo.setVisibility(View.INVISIBLE);
			}
		});
		explorer.show();
	}

	private void showRemoteExplorer(){
		currentDir = DIVIDER;
		final Dialog explorer = new Dialog(context);
		explorer.setTitle("Explorer");
		explorer.requestWindowFeature(Window.FEATURE_NO_TITLE);
		explorer.setContentView(R.layout.local_explorer);
		explorer.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		explorer.setCanceledOnTouchOutside(false);

		LayoutParams params = explorer.getWindow().getAttributes();

		Display display = getWindowManager().getDefaultDisplay();
		Point p = new Point();
		display.getSize(p);
		int defHeight = p.y;

		params.height = (int) (0.70 * defHeight);

		remoteList = (ListView) explorer.findViewById(R.id.list_explorer);
		progress = (LinearLayout) explorer.findViewById(R.id.loading);
		title = (TextView) explorer.findViewById(R.id.path);
		undo = (ImageView) explorer.findViewById(R.id.undo);
		selectFol = (Button) explorer.findViewById(R.id.select);
		createFol = (Button) explorer.findViewById(R.id.create);
		selectAll = (Button) explorer.findViewById(R.id.select_all);

		remoteExplorer = new RemoteExplorer();
		currentDir = DIVIDER;
		remoteExplorer.execute(currentDir);

		selectAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				remoteAdapter.setCheckBoxVisibilty(true);
				int count = remoteAdapter.getCount();
				for(int i = 0; i < count; i++){
					remoteAdapter.setChecked(i, true);
				}
				remoteAdapter.notifyDataSetChanged();
				undo.setVisibility(View.VISIBLE);
			}
		});

		selectFol.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * get and 
				 * store the paths
				 * to sync..
				 */
				remoteSync.clear();
				List<CheckBoxState> checked = remoteAdapter.getCheckedItems();

				for(int i = 0; i < checked.size(); i++){
					CheckBoxState item = checked.get(i);

					if(item.isChecked()){
						if(remoteSync.contains(item.getItem().getPath()) == false)
							remoteSync.add(item.getItem().getPath());
					}
				}
				explorer.dismiss();
				if(remoteSync.isEmpty() == false){
					remoteBrowseDone.setVisibility(View.VISIBLE);
					remoteBrowse.setText("Selected");
				}
			}
		});

		createFol.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String path = title.getText().toString();

				AlertDialog.Builder folder = new AlertDialog.Builder(context);
				folder.setTitle("Create Folder");
				LinearLayout layout = new LinearLayout(context);
				layout.setOrientation(LinearLayout.HORIZONTAL);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.setMargins(10, 10, 10, 10);

				layout.setLayoutParams(params);

				TextView msg = new TextView(context);
				msg.setText("Folder name: ");
				layout.addView(msg);

				final EditText folderName = new EditText(context);
				params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f);
				folderName.setLayoutParams(params);
				folderName.setInputType(InputType.TYPE_CLASS_TEXT);
				layout.addView(folderName);

				folder.setCancelable(false);
				folder.setNegativeButton("Cancel", null);

				folder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String folder = folderName.getText().toString();

						boolean created = false;
						try {
							created = ftpClient.makeDirectory(path + DIVIDER + folder);
						} catch (IOException e) {
							e.printStackTrace();
						}

						if(created == false){
							Toast.makeText(context, "can not create folder, access denied", Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(context, "folder created", Toast.LENGTH_SHORT).show();
						}
					}
				});
				folder.show();
			}
		});

		remoteList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				boolean isCheckBoxVisible = remoteAdapter.getCheckBoxVisibilty();
				ExplorerItem item = remoteAdapter.getItem(position);

				if(isCheckBoxVisible){

					if(item.getImage().equalsIgnoreCase("folder") || item.getImage().equalsIgnoreCase("file")){
						remoteAdapter.setChecked(position, !remoteAdapter.getChecked(position));
						remoteAdapter.notifyDataSetChanged();
					}else {
						remoteExplorer = new RemoteExplorer();
						remoteExplorer.execute(item.getPath());
					}
				}else{
					if(item.getImage().equalsIgnoreCase("folder") || item.getImage().equalsIgnoreCase("direc_up")){
						remoteExplorer = new RemoteExplorer();
						remoteExplorer.execute(item.getPath());
					}else{
						onFileClick(item);
					}
				}
			}
		});

		remoteList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				ExplorerItem item = remoteAdapter.getItem(position); 

				if(item.getImage().equalsIgnoreCase("direc_up")){
					return true;
				}
				else{
					if(remoteAdapter.getCheckBoxVisibilty() == true)
						return true;
					remoteAdapter.setChecked(position, true);
					remoteAdapter.setCheckBoxVisibilty(true);
					remoteAdapter.notifyDataSetChanged();
				}
				return true;
			}
		});

		undo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(remoteAdapter.getCheckBoxVisibilty() == true){
					int count = remoteAdapter.getCount();
					for(int i = 0; i < count; i++){
						remoteAdapter.setChecked(i, false);
					}
					remoteAdapter.setCheckBoxVisibilty(false);
					remoteAdapter.notifyDataSetChanged();
				}
				undo.setVisibility(View.INVISIBLE);
			}
		});
		explorer.show();
	}

	private void checkForDirectory(String path) {
		File rootDir = new File(localRoot);
		if (rootDir.getAbsolutePath().equalsIgnoreCase(path)) {
			selectFol.setEnabled(false);
			createFol.setEnabled(false);
			selectAll.setEnabled(false);
		} else {
			selectFol.setEnabled(true);
			createFol.setEnabled(true);
			selectAll.setEnabled(true);
		}
	}

	@Override
	public void onBackPressed() {
		if(progress != null && progress.getVisibility() == View.VISIBLE)
			remoteExplorer.cancel(true);
		super.onBackPressed();
		overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if(id == android.R.id.home){
			NavUtils.navigateUpFromSameTask(this);
			overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private class RemoteExplorer extends AsyncTask<String, Void, List<ExplorerItem>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			remoteList.setVisibility(View.GONE);
			progress.setVisibility(View.VISIBLE);
			title.setText(currentDir);
		}

		@Override
		protected List<ExplorerItem> doInBackground(String... params) {
			String directory = params[0];
			currentDir = directory;
			if(ftpUtils == null)
				ftpUtils = new FTPUtils();

			ftpClient = ftpUtils.getFTPClient();

			String server = serverAdd.getText().toString();
			String port = serverPort.getText().toString();
			int portNo = -1;
			if(port != null && port.equals("") == false)
				portNo = Integer.parseInt(port);

			String userId = serverUserId.getText().toString();
			String password = serverPass.getText().toString();

			if(ftpClient.isConnected() == false)
				ftpUtils.connectAndLogin(server, portNo, userId, password);

			FTPFile[] dirs = null;
			try {
				if(directory.equals("/"))
					dirs = ftpClient.listFiles();
				else
					dirs = ftpClient.listFiles(directory);
			} catch (IOException e) {
				e.printStackTrace();
			}

			List<ExplorerItem> dir = new ArrayList<ExplorerItem>();
			List<ExplorerItem> files = new ArrayList<ExplorerItem>();

			try{
				for(FTPFile ff : dirs){
					//this is test..
					if(ff.getName().startsWith(".") || !ff.hasPermission(FTP.ASCII_FILE_TYPE, FTPFile.READ_PERMISSION)){
						continue;
					}
					String dateModified = "";
					Date date = ff.getTimestamp().getTime();
					dateModified = date.toString();
					if(ff.isDirectory()){
						FTPFile[] fileBuff = ftpClient.listFiles(currentDir + DIVIDER + ff.getName());
						int buf = 0;
						if(fileBuff != null){
							/*
							 * here we get the no of files
							 * within the directory
							 */
							buf = fileBuff.length;
						} else
							buf = 0;
						String numItems = String.valueOf(buf);
						if(buf <= 1)
							numItems = numItems + " item";
						else
							numItems = numItems + " items";
						/*
						 * add these details
						 * to the main dir list
						 */
						String folderName = "";
						String[] name = ff.getName().split(DIVIDER);
						folderName = name[name.length-1];

						if(currentDir.equals(DIVIDER))
							dir.add(new ExplorerItem(folderName, numItems, dateModified, currentDir + ff.getName(), "folder"));
						else
							dir.add(new ExplorerItem(folderName, numItems, dateModified, currentDir + DIVIDER + ff.getName(), "folder"));
					} else{
						/*
						 * add these details
						 * to the main files list
						 */
						String fileLength = "";
						if(ff.getSize() > 1)
							fileLength = ff.getSize() + " bytes";
						else
							fileLength = ff.getSize() + " byte";

						String fileName = "";
						String[] name = ff.getName().split(DIVIDER);
						fileName = name[name.length-1];

						if(currentDir.equals(DIVIDER))
							files.add(new ExplorerItem(fileName, fileLength, dateModified, currentDir + ff.getName(), "file"));
						else
							files.add(new ExplorerItem(fileName, fileLength, dateModified, currentDir + DIVIDER + ff.getName(), "file"));
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			/*
			 * Sort the list view
			 */
			Collections.sort(files);
			Collections.sort(dir);
			dir.addAll(files);

			if(!directory.equalsIgnoreCase(DIVIDER) && !directory.equals("")){
				String[] oldDirectory = directory.split(DIVIDER);

				String directoryParent = "";

				for(int i = 0; i < oldDirectory.length - 1; i++){
					if(oldDirectory[i].equals("") == false)
						directoryParent = directoryParent + DIVIDER + oldDirectory[i];
				}
				if(directoryParent.equals(""))
					directoryParent = DIVIDER;

				//				currentDir = directoryParent;

				dir.add(0, new ExplorerItem("..", "Up Directory", "", directoryParent, "direc_up"));
			}
			return dir;
		}

		@Override
		protected void onPostExecute(List<ExplorerItem> result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			remoteList.setVisibility(View.VISIBLE);
			title.setText(currentDir);
			remoteAdapter = new FileExplorerAdapter(context, R.layout.row_explorer, result);
			remoteList.setAdapter(remoteAdapter);
		}
	}

	private void fill(File directory) {
		checkForDirectory(directory.getAbsolutePath());
		File[] dirs = directory.listFiles();

		title.setText(directory.getAbsolutePath());

		List<ExplorerItem> dir = new ArrayList<ExplorerItem>();
		List<ExplorerItem> files = new ArrayList<ExplorerItem>();

		try {
			for (File ff : dirs) {
				// this is test..
				if (ff.isHidden() || !ff.canRead()) {
					continue;
				}
				Date lastModifiedDate = new Date(ff.lastModified());
				DateFormat formater = DateFormat.getDateTimeInstance();
				String dateModified = formater.format(lastModifiedDate);

				if (ff.isDirectory()) {
					File[] fileBuff = ff.listFiles();
					int buf = 0;
					if (fileBuff != null) {
						/*
						 * here we get the no of files within the directory
						 */
						buf = fileBuff.length;
					} else
						buf = 0;
					String numItems = String.valueOf(buf);
					if (buf <= 1)
						numItems = numItems + " item";
					else
						numItems = numItems + " items";
					/*
					 * add these details to the main dir list
					 */
					if (directory.getName().equalsIgnoreCase("mnt")) {
						if (ff.getName().equalsIgnoreCase("sdcard")
								|| ff.getName().equalsIgnoreCase("extsdcard")) {

							String folderName = "";
							String image = "";
							if (ff.getName().equalsIgnoreCase("sdcard")) {
								folderName = "Internal Memory";
								image = "internal";
							} else {
								folderName = "Memory Card";
								image = "memory_card";
							}
							dir.add(new ExplorerItem(folderName, numItems,
									dateModified, ff.getAbsolutePath(), image));
						} else
							continue;
					} else
						dir.add(new ExplorerItem(ff.getName(), numItems,
								dateModified, ff.getAbsolutePath(), "folder"));
				} else {
					/*
					 * add these details to the main files list
					 */
					String fileLength = "";
					if (ff.length() > 1)
						fileLength = ff.length() + " bytes";
					else
						fileLength = ff.length() + " byte";

					files.add(new ExplorerItem(ff.getName(), fileLength,
							dateModified, ff.getAbsolutePath(), "file"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * Sort the list view
		 */
		Collections.sort(files);
		Collections.sort(dir);
		dir.addAll(files);

		if (!directory.getName().equalsIgnoreCase("sdcard")
				|| !directory.getName().equalsIgnoreCase("extsdcard")) {
			dir.add(0, new ExplorerItem("..", "Up Directory", "", directory.getParent(), "direc_up"));

			if (directory.getName().equalsIgnoreCase("mnt")) {
				dir.remove(0);
			}
		}

		localAdapter = new FileExplorerAdapter(getApplicationContext(), R.layout.row_explorer, dir);
		localList.setAdapter(localAdapter);
	}

	private void onFileClick(ExplorerItem item) {
		Toast.makeText(context, item.getName(), Toast.LENGTH_SHORT).show();
	}

	private class SaveToDatabase extends AsyncTask<Void, Void, Void>{

		private FTPCredentials cred;

		public SaveToDatabase(FTPCredentials cred){
			this.cred = cred;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(saving == null)
				saving = new ProgressDialog(context);
			saving = ProgressDialog.show(context, null, "Saving...", true, false);
		}

		@Override
		protected Void doInBackground(Void... params) {
			if(dataManager == null)
				dataManager = new DataManager();
			dataManager.setWritableState();

			ContentValues values = new Utilities().createFTPCredentials(cred);

			//add credentials..
			id = dataManager.addToDatabase(Constants.FTP_CRED_TABLE, values);

			values.clear();
			localFiles.clear();
			//add local sync files
			for(int i = 0; i < localSync.size(); i++){

				values.put(Constants.SYNC_ACCOUNT_NAME, cred.getAccountName());
				values.put(Constants.SYNC_PATH, localSync.get(i));
				dataManager.addToDatabase(Constants.FTP_LOCAL_PATHS, values);
				values.clear();
			}

			values.clear();
			remoteFiles.clear();
			//add remote sync files
			for(int i = 0; i < remoteSync.size(); i++){

				values.put(Constants.SYNC_ACCOUNT_NAME, cred.getAccountName());
				values.put(Constants.SYNC_PATH, remoteSync.get(i));
				dataManager.addToDatabase(Constants.FTP_REMOTE_PATHS, values);
				values.clear();
			}
			//close connection..
			dataManager.close();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			//send broadcast..
			Intent sync = new Intent(context, AlarmReceiver.class);
			sync.setAction(cred.getAccountName());
			sync.putExtra("id", id);
			sync.putExtra("interval", cred.getSyncInterval());
			sendBroadcast(sync);
			if(saving.isShowing())
				saving.dismiss();
			Toast.makeText(context, "Saved in database", Toast.LENGTH_SHORT).show();
		}
	}

	private class FocusListener implements OnFocusChangeListener {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			int id = v.getId();
			String text = null;
			switch (id) {
			case R.id.ftp_account_name:
				text = accountName.getText().toString();
				if (text != null && !text.equals("") && !text.equals(" ")) {
					nameDone.setVisibility(View.VISIBLE);
				} else
					nameDone.setVisibility(View.INVISIBLE);
				break;

			case R.id.ftp_server:
				text = serverAdd.getText().toString();
				if (text != null && !text.equals("") && !text.equals(" ")) {
					serverDone.setVisibility(View.VISIBLE);
				} else
					serverDone.setVisibility(View.INVISIBLE);
				break;

			case R.id.ftp_port:
				text = serverPort.getText().toString();
				if (text != null && !text.equals("") && !text.equals(" ")) {
					portDone.setVisibility(View.VISIBLE);
				} else
					portDone.setVisibility(View.INVISIBLE);
				break;

			case R.id.ftp_username:
				text = serverUserId.getText().toString();
				if (text != null && !text.equals("") && !text.equals(" ")) {
					userDone.setVisibility(View.VISIBLE);
				} else
					userDone.setVisibility(View.INVISIBLE);
				break;

			case R.id.ftp_pass:
				text = serverPass.getText().toString();
				if (text != null && !text.equals("") && !text.equals(" ")) {
					passDone.setVisibility(View.VISIBLE);
				} else
					passDone.setVisibility(View.INVISIBLE);
				break;
			}
		}
	}
}