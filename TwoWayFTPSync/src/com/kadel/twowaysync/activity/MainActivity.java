package com.kadel.twowaysync.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.kadel.twowayftpsync.R;
import com.kadel.twowaysync.db.DataManager;

public class MainActivity extends Activity{

	private Button addAccount, viewAccount;
	private DataManager dataManager;
	private SharedPreferences shared;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		shared = getSharedPreferences("application_pref", Context.MODE_PRIVATE);
		boolean installed = false;
		if(shared.contains("installed")){
			installed = shared.getBoolean("installed", false);
		}
		if(installed == false){
			Intent shortcutIntent = new Intent(this, MainActivity.class);
			shortcutIntent.setAction(Intent.ACTION_MAIN);
			Intent intent = new Intent();
			intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
			intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Two way FTP Sync");
			intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher));
			intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
			sendBroadcast(intent);
			
			Editor edit = shared.edit();
			edit.putBoolean("installed", true);
			edit.commit();
		}
		
		dataManager = new DataManager();
		addAccount = (Button) findViewById(R.id.add_account);
		viewAccount = (Button) findViewById(R.id.view_account);
		
		addAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent main = new Intent(MainActivity.this, Main.class);
				startActivity(main);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
		
		viewAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent listAccount = new Intent(MainActivity.this, ListAccount.class);
				startActivity(listAccount);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		dataManager.close();
	}
}
