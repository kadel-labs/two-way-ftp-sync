package com.kadel.twowaysync.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.beans.FileMetaDeta;
import com.kadel.twowaysync.utils.Constants;

public class DataManager {

	private static Context context;
	private MyDBHelper helper;
	private static SQLiteDatabase database;
	
	public DataManager(){
		super();
	}
	
	public DataManager(Context context){
		DataManager.context = context;
		helper = new MyDBHelper(context);
	}
	
	/**
	 * set the writable state of database
	 */
	public void setWritableState(){
		if(helper != null){
			if(database == null)
				database = helper.getWritableDatabase();
			else{
				if(database.isReadOnly())
					database = helper.getWritableDatabase();
			}
		}else{
			helper = new MyDBHelper(context);
			database = helper.getWritableDatabase();
		}
	}
	
	/**
	 * close the database
	 */
	public void close(){
		if(database != null){
			if(database.isOpen())
				database.close();
		}
	}
	
	public Context getContext(){
		return context;
	}
	
	/**
	 * generic method defined to add to any table in db
	 */
	public synchronized long addToDatabase(String tableName, ContentValues values){
		return database.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_REPLACE);
	}
	
	/**
	 * update any database table using this method
	 */
	public synchronized void updateDatabase(String tableName, ContentValues values, String keyValue, String key, String keyValue2,
			String key2){
		if(keyValue2 == null || key2 == null)
			database.updateWithOnConflict(tableName, values, key + " = ?", new String[]{keyValue}, SQLiteDatabase.CONFLICT_REPLACE);
		else
			database.updateWithOnConflict(tableName, values, key + " = ? and " + key2 + " = ?", new String[]{keyValue, keyValue2}, 
					SQLiteDatabase.CONFLICT_REPLACE);
	}
	
	/**
	 * delete contents of table
	 */
	public synchronized void deleteContent(String tableName, String key, String keyValue){
		database.delete(tableName, key + " = ? ", new String[]{keyValue});
	}
	
	/**
	 * get FTP credentials, null if no entries found
	 */
	public synchronized List<FTPCredentials> getFTPCredentials(String accountName){
		List<FTPCredentials> credentials = new ArrayList<FTPCredentials>();
		
		Cursor cursor = null;
		if(accountName == null)
			cursor = database.rawQuery("select * from " + Constants.FTP_CRED_TABLE, null);
		else
			cursor = database.rawQuery("select * from " + Constants.FTP_CRED_TABLE + " where " + Constants.FTP_NAME + 
					" = ?", new String[]{accountName});
		cursor.moveToFirst();
		
		if(cursor.getCount() > 0){
			while(!cursor.isAfterLast()){
				FTPCredentials cred = new FTPCredentials();
				cred.setAccountName(cursor.getString(0));
				cred.setServer(cursor.getString(1));
				cred.setPort(cursor.getInt(2));
				cred.setUserName(cursor.getString(3));
				cred.setPassword(cursor.getString(4));
				cred.setSyncInterval(cursor.getLong(5));
				cred.setSyncOnWifi(cursor.getInt(6));
				cred.setLocalSyncScheme(cursor.getInt(7));
				cred.setLocalSyncMode(cursor.getInt(8));
				cred.setRemoteSyncScheme(cursor.getInt(9));
				cred.setRemoteSyncMode(10);
				cred.setSyncStatus(cursor.getString(11));
				cred.setLastSync(cursor.getString(12));
				
				credentials.add(cred);
				cursor.moveToNext();
			}
		}
		return credentials;
	}
	
	/**
	 * get the FTP sync details from local storage, all if accountName is null, null if no entries
	 * found
	 */
	public synchronized List<FileMetaDeta> getFTPLocalSync(String accountName){
		List<FileMetaDeta> files = new ArrayList<FileMetaDeta>();
		
		Cursor cursor = null;
		if(accountName == null)
			cursor = database.rawQuery("select * from " + Constants.FTP_LOCAL_PATHS, null);
		else
			cursor = database.rawQuery("select * from " + Constants.FTP_LOCAL_PATHS + " where " + Constants.SYNC_ACCOUNT_NAME + 
					" = ?", new String[]{accountName});
		
		cursor.moveToFirst();
		
		if(cursor.getCount() > 0){
			while(!cursor.isAfterLast()){
				FileMetaDeta file = new FileMetaDeta();
				file.setSyncAccount(cursor.getString(0));
				file.setSyncPath(cursor.getString(1));
				
				files.add(file);
				cursor.moveToNext();
			}
		}
		return files;
	}
	
	/**
	 * get the FTP sync details from remote storage, all if accountName is null, null if no entries
	 * found
	 */
	public synchronized List<FileMetaDeta> getFTPRemoteSync(String accountName){
		List<FileMetaDeta> files = new ArrayList<FileMetaDeta>();
		
		Cursor cursor = null;
		if(accountName == null)
			cursor = database.rawQuery("select * from " + Constants.FTP_REMOTE_PATHS, null);
		else
			cursor = database.rawQuery("select * from " + Constants.FTP_REMOTE_PATHS + " where " + Constants.SYNC_ACCOUNT_NAME + 
					" = ?", new String[]{accountName});
		
		cursor.moveToFirst();
		
		if(cursor.getCount() > 0){
			while(!cursor.isAfterLast()){
				FileMetaDeta file = new FileMetaDeta();
				file.setSyncAccount(cursor.getString(0));
				file.setSyncPath(cursor.getString(1));
				
				files.add(file);
				cursor.moveToNext();
			}
		}
		return files;
	}
}