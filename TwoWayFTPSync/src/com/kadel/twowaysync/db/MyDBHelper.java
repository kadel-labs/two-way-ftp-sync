package com.kadel.twowaysync.db;

import com.kadel.twowaysync.utils.Constants;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper extends SQLiteOpenHelper{
	
	public MyDBHelper(Context context){
		super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		/*
		 * creating tables for the
		 * first time...
		 */
		db.execSQL(Constants.CREATE_FTP_CRED_TABLE);
		db.execSQL(Constants.CREATE_FTP_LOCAL_PATHS);
		db.execSQL(Constants.CREATE_FTP_REMOTE_PATHS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/*
		 * on version change of database
		 * you can drop/create new tables..
		 */
	}

}