package com.kadel.twowaysync.adapter;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.kadel.twowayftpsync.R;
import com.kadel.twowaysync.beans.CheckBoxState;
import com.kadel.twowaysync.beans.ExplorerItem;

public class FileExplorerAdapter extends ArrayAdapter<ExplorerItem>{

	private Context mContext;
	private int id;
	private List<ExplorerItem> items;
	private List<CheckBoxState> checkBoxState;
	private boolean checkboxVisibility = false;

	public FileExplorerAdapter(Context mContext, int textViewResourceId, List<ExplorerItem> objects){
		super(mContext, textViewResourceId, objects);
		this.mContext = mContext;
		id = textViewResourceId;
		items = objects;
		checkBoxState = new ArrayList<CheckBoxState>();
		for(ExplorerItem item : items){
			checkBoxState.add(new CheckBoxState(item, false));
		}
	}

	public ExplorerItem getItem(int index){
		return items.get(index);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if(view == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(id, null);
		}
		final ExplorerItem item = items.get(position);
		if(item != null){
			TextView name = (TextView) view.findViewById(R.id.name_of_item);
			TextView noOfItems = (TextView) view.findViewById(R.id.no_of_files);
			TextView modifiedDate = (TextView) view.findViewById(R.id.last_modified_date_time);
			ImageView image = (ImageView) view.findViewById(R.id.folder_image);
			final CheckBox checkBox = (CheckBox) view.findViewById(R.id.file_check);

			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					checkBoxState.get(position).setChecked(isChecked);
					checkBox.setChecked(isChecked);
				}
			});

			String uri = "drawable/" + item.getImage();
			int imageResource = mContext.getResources().getIdentifier(uri, null, mContext.getPackageName());
			Drawable drawImage = mContext.getResources().getDrawable(imageResource);
			image.setImageDrawable(drawImage);

			if(name != null)
				name.setText(item.getName());
			if(noOfItems != null)
				noOfItems.setText(item.getData());
			if(modifiedDate != null)
				modifiedDate.setText(item.getDate());

			if(item.getImage().equalsIgnoreCase("folder") || item.getImage().equalsIgnoreCase("file")){
				if(checkboxVisibility)
					checkBox.setVisibility(View.VISIBLE);
				else {
					checkBox.setVisibility(View.INVISIBLE);
				}
			}else
				checkBox.setVisibility(View.INVISIBLE);
			checkBox.setChecked(checkBoxState.get(position).isChecked());
		}
		return view;
	}

	public void setCheckBoxVisibilty(boolean visibility){
		checkboxVisibility = visibility;
	}

	public boolean getCheckBoxVisibilty(){
		return checkboxVisibility;
	}

	public void setChecked(int position, boolean checked){
		checkBoxState.get(position).setChecked(checked);
	}
	
	public boolean getChecked(int position){
		return checkBoxState.get(position).isChecked();
	}

	public List<CheckBoxState> getCheckedItems(){
		return checkBoxState;
	}
}