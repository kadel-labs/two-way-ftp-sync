package com.kadel.twowaysync.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kadel.twowayftpsync.R;
import com.kadel.twowaysync.beans.FTPCredentials;

public class FTPListAdapter extends ArrayAdapter<FTPCredentials>{

	private List<FTPCredentials> items;
	private LayoutInflater inflater;
	private int layout;

	public FTPListAdapter(Context context, int resource, List<FTPCredentials> objects) {
		super(context, resource, objects);
		items = objects;
		layout = resource;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		ViewHolder holder = null;
		FTPCredentials item = items.get(position);
		if(view == null){
			holder = new ViewHolder();
			view = inflater.inflate(layout, parent, false);
			holder.accountImage = (ImageView) view.findViewById(R.id.account_image);
			holder.accountName = (TextView) view.findViewById(R.id.account_name);
			holder.accountSyncTime = (TextView) view.findViewById(R.id.account_sync_time);
			holder.status = (Button) view.findViewById(R.id.account_sync_status);

			view.setTag(holder);
		}else{
			holder = (ViewHolder) view.getTag();
		}
		holder.accountImage.setBackgroundResource(R.drawable.ftp_account);
		holder.accountName.setText(item.getAccountName());
		if(item.getLastSync() != null)
			holder.accountSyncTime.setText(item.getLastSync());
		else
			holder.accountSyncTime.setText("no history");

		holder.status.setText(item.getSyncStatus());
		return view;
	}

	private class ViewHolder{
		private ImageView accountImage;
		private TextView accountName, accountSyncTime;
		private Button status;
	}
}