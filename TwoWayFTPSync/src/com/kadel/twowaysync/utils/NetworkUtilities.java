package com.kadel.twowaysync.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

public class NetworkUtilities {

	private static final String[] NETWORKS = {"2G/EDGE", "3G", "4G"};
	
	public String getNetworkClass(Context context){
		TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		
	    int networkType = telManager.getNetworkType();
	    
	    switch (networkType) {
	        case TelephonyManager.NETWORK_TYPE_GPRS:
	        case TelephonyManager.NETWORK_TYPE_EDGE:
	        case TelephonyManager.NETWORK_TYPE_CDMA:
	        case TelephonyManager.NETWORK_TYPE_1xRTT:
	        case TelephonyManager.NETWORK_TYPE_IDEN:
	            return NETWORKS[0];
	            
	        case TelephonyManager.NETWORK_TYPE_UMTS:
	        case TelephonyManager.NETWORK_TYPE_EVDO_0:
	        case TelephonyManager.NETWORK_TYPE_EVDO_A:
	        case TelephonyManager.NETWORK_TYPE_HSDPA:
	        case TelephonyManager.NETWORK_TYPE_HSUPA:
	        case TelephonyManager.NETWORK_TYPE_HSPA:
	        case TelephonyManager.NETWORK_TYPE_EVDO_B:
	        case TelephonyManager.NETWORK_TYPE_EHRPD:
	        case TelephonyManager.NETWORK_TYPE_HSPAP:
	            return NETWORKS[1];
	            
	        case TelephonyManager.NETWORK_TYPE_LTE:
	            return NETWORKS[2];
	            
	        default:
	            return null;
	    }
	}
}
