package com.kadel.twowaysync.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SavePreferences {

	private SharedPreferences sharedPreferences;
	@SuppressWarnings("unused")
	private Editor edit;
	private static final String PREFERENCES = "sync_prefs";
	
	public SavePreferences(Context context){
		sharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
	}
	
	public Editor getEditor(){
		return sharedPreferences.edit();
	}
}
