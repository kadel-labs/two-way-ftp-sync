package com.kadel.twowaysync.utils;

public class Constants {

	public static final String DATABASE_NAME = "sync_database.db";
	public static final int DATABASE_VERSION = 1;
	
	public static final String FTP_CRED_TABLE = "ftp_credentials_table";
	public static final String FTP_NAME = "ftp_name";
	public static final String FTP_SERVER = "ftp_server";
	public static final String FTP_PORT = "ftp_port";
	public static final String FTP_USERNAME = "ftp_username";
	public static final String FTP_PASSWORD = "ftp_password";
	public static final String FTP_SYNC_INTERVAL = "ftp_sync_interval";	
	public static final String FTP_SYNC_WIFI = "ftp_sync_wifi";
	public static final String FTP_LOCAL_SYNC_SCHEME = "ftp_local_scheme";
	public static final String FTP_REMOTE_SYNC_SCHEME = "ftp_remote_scheme";
	public static final String FTP_LOCAL_SYNC_MODE = "ftp_local_mode";
	public static final String FTP_REMOTE_SYNC_MODE = "ftp_remote_mode";
	public static final String FTP_SYNC_STATUS = "ftp_sync_status";
	public static final String FTP_LAST_SYNC = "ftp_last_sync";
	
	public static final String FTP_LOCAL_PATHS = "ftp_local_sync";
	public static final String FTP_REMOTE_PATHS = "ftp_remote_sync";
	
	public static final String SYNC_ACCOUNT_NAME = "sync_account_name";
	public static final String SYNC_PATH = "sync_path";
	
	public static final String CREATE_FTP_CRED_TABLE = "create table if not exists " + FTP_CRED_TABLE + "(" + FTP_NAME + 
		" text not null unique, " + FTP_SERVER + " text not null, " + FTP_PORT + " integer, "
		+ FTP_USERNAME + " text not null, " + FTP_PASSWORD + " text not null, " + FTP_SYNC_INTERVAL + " real not null, " + 
		FTP_SYNC_WIFI + " int not null, " + FTP_LOCAL_SYNC_SCHEME + " integer, " + FTP_LOCAL_SYNC_MODE + " integer, " + 
		FTP_REMOTE_SYNC_SCHEME + " integer, " + FTP_REMOTE_SYNC_MODE + " integer, " + FTP_SYNC_STATUS + " text not null, " + 
		FTP_LAST_SYNC + " text, " + " unique(" + FTP_SERVER + ", " + FTP_PORT + ", " + FTP_USERNAME + ", " + FTP_PASSWORD + "));";
	
	public static final String CREATE_FTP_LOCAL_PATHS = "create table if not exists " + FTP_LOCAL_PATHS + "(" + SYNC_ACCOUNT_NAME + 
		" text not null, " + SYNC_PATH + " text, " + " unique(" + SYNC_ACCOUNT_NAME + ", " + SYNC_PATH + "));";
	
	public static final String CREATE_FTP_REMOTE_PATHS = "create table if not exists " + FTP_REMOTE_PATHS + "(" + SYNC_ACCOUNT_NAME + 
		" text not null, " + SYNC_PATH + " text, " + " unique(" + SYNC_ACCOUNT_NAME + ", " + SYNC_PATH + "));";
}
