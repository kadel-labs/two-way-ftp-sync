package com.kadel.twowaysync.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import android.content.ContentValues;
import com.kadel.twowaysync.beans.FTPCredentials;
import com.kadel.twowaysync.beans.SyncFileDetails;

public class Utilities {

	private List<SyncFileDetails> localFiles;
	private List<SyncFileDetails> remoteFiles;
	private static final String DIVIDER = "/";
	
	public Utilities(){
		localFiles = new ArrayList<SyncFileDetails>();
		remoteFiles = new ArrayList<SyncFileDetails>();
	}

	public List<SyncFileDetails> getLocalFiles(int scheme, String path, String rootPath){
		File folder = new File(path);

		if(folder.isFile() && folder.isHidden() == false){
			SyncFileDetails detail = new SyncFileDetails();
			detail.setFileName(folder.getName());
			detail.setPath(folder.getAbsolutePath());
			detail.setSyncPath(DIVIDER);
			localFiles.add(detail);
		}
		if(folder.isDirectory() && folder.isHidden() == false){
			File[] local = folder.listFiles();

			for(int i = 0; i < local.length; i++){
				if(local[i].isDirectory() && local[i].isHidden() == false){
					/*
					 * get files within folder if 
					 * only scheme applied..
					 */
					if(scheme == 1)
						getLocalFiles(scheme, local[i].getAbsolutePath(), rootPath);
					else
						continue;
				}else if(local[i].isFile() && local[i].isHidden() == false){
					SyncFileDetails detail = new SyncFileDetails();
					detail.setFileName(local[i].getName());
					detail.setPath(local[i].getAbsolutePath());

					String[] s = detail.getPath().split(rootPath);
					String[] s1 = rootPath.split(DIVIDER);
					String syncPath = s1[s1.length-1] + s[s.length-1];
					detail.setSyncPath(syncPath);
					localFiles.add(detail);
				}
			}
		}
		return localFiles;
	}
	
	public List<SyncFileDetails> getRemoteFiles(int scheme, String path, String rootPath, FTPClient ftpClient){
		try {
			FTPFile file = ftpClient.mlistFile(path);
			
			if(file != null && file.isFile() && file.hasPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION)
					&& file.getName() != null && file.getName().startsWith(".") == false){
				SyncFileDetails detail = new SyncFileDetails();
				String[] s = file.getName().split(DIVIDER);
				detail.setFileName(s[s.length-1]);
				detail.setPath(file.getName());
				detail.setSyncPath("/");
				remoteFiles.add(detail);
			}
			if( file != null && file.isDirectory() && file.hasPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION)
					&& file.getName() != null && file.getName().startsWith(".") == false){
				FTPFile[] files = ftpClient.listFiles(file.getName());
				
				for(int i = 0; i < files.length; i++){
					if(files[i].isDirectory() && files[i].hasPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION)
							&& files[i].getName().startsWith(".") == false){
						
						if(scheme == 1)
							getRemoteFiles(scheme, files[i].getName(), rootPath, ftpClient);
						else
							continue;
					}else if(files[i].isFile() && files[i].hasPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION)){
						SyncFileDetails detail = new SyncFileDetails();
						String[] s = files[i].getName().split(DIVIDER);
						detail.setFileName(s[s.length-1]);
						detail.setPath(file.getName() + DIVIDER + files[i].getName());
						
						s = files[i].getName().split(rootPath);
						String[] s1 = rootPath.split(DIVIDER);
						String syncPath = s1[s1.length-1] + DIVIDER + s[s.length-1];
						detail.setSyncPath(syncPath);
						remoteFiles.add(detail);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return remoteFiles;
	}
	
	public ContentValues createFTPCredentials(FTPCredentials cred){
		ContentValues values = new ContentValues();
		values.put(Constants.FTP_NAME, cred.getAccountName());
		values.put(Constants.FTP_SERVER, cred.getServer());
		values.put(Constants.FTP_PORT, cred.getPort());
		values.put(Constants.FTP_USERNAME, cred.getUserName());
		values.put(Constants.FTP_PASSWORD, cred.getPassword());
		values.put(Constants.FTP_SYNC_INTERVAL, cred.getSyncInterval());
		values.put(Constants.FTP_SYNC_WIFI, cred.getSyncOnWifi());
		values.put(Constants.FTP_LOCAL_SYNC_SCHEME, cred.getLocalSyncScheme());
		values.put(Constants.FTP_LOCAL_SYNC_MODE, cred.getLocalSyncMode());
		values.put(Constants.FTP_REMOTE_SYNC_SCHEME, cred.getRemoteSyncScheme());
		values.put(Constants.FTP_REMOTE_SYNC_MODE, cred.getRemoteSyncMode());
		values.put(Constants.FTP_SYNC_STATUS, cred.getSyncStatus());
		values.put(Constants.FTP_LAST_SYNC, cred.getLastSync());
		return values;
	}
}