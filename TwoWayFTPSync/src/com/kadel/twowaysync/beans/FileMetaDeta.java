package com.kadel.twowaysync.beans;

public class FileMetaDeta {

	private String accountName;
	private String syncPath;
	
	public String getSyncAccount() {
		return accountName;
	}
	
	public void setSyncAccount(String accountName) {
		this.accountName = accountName;
	}
	
	public String getSyncPath() {
		return syncPath;
	}
	
	public void setSyncPath(String path) {
		this.syncPath = path;
	}
}