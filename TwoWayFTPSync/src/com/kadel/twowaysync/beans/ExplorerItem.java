package com.kadel.twowaysync.beans;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class ExplorerItem implements Comparable<ExplorerItem>{

	private String name;
	private String details;
	private String date;
	private String path;
	private String image;
	
	public ExplorerItem(String name, String data, String date, String path, String image){
		this.name = name;
		this.details = data;
		this.date = date;
		this.path = path;
		this.image = image;
	}
	
	public String getName() {
		return name;
	}

	public String getData() {
		return details;
	}

	public String getDate() {
		return date;
	}

	public String getPath() {
		return path;
	}

	public String getImage() {
		return image;
	}
	
	@Override
	public int compareTo(ExplorerItem itemToCompare) {
		if(this.name != null)
			return this.name.toLowerCase().compareTo(itemToCompare.getName().toLowerCase());
		else
			throw new IllegalArgumentException();
	}
}