package com.kadel.twowaysync.beans;

public class CheckBoxState {
	
	private ExplorerItem item;
	private boolean checked;
	
	public CheckBoxState(ExplorerItem item, boolean checked){
		this.setItem(item);
		this.checked = checked;
	}
	
	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public ExplorerItem getItem() {
		return item;
	}

	public void setItem(ExplorerItem item) {
		this.item = item;
	}
}