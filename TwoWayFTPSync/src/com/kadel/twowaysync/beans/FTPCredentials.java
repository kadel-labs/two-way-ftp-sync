package com.kadel.twowaysync.beans;

public class FTPCredentials {

	private String accountName;
	private String server;
	private int port;
	private String userName;
	private String password;
	private long syncInterval;
	private int wifiOnly;
	private int localSyncScheme;
	private int localSyncMode;
	private int remoteSyncScheme;
	private int remoteSyncMode;
	private String syncStatus;
	private String lastSync;
	
	public String getAccountName() {
		return accountName;
	}
	
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public String getServer() {
		return server;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public long getSyncInterval() {
		return syncInterval;
	}
	
	public void setSyncInterval(long syncInterval) {
		this.syncInterval = syncInterval;
	}
	
	public int getLocalSyncScheme() {
		return localSyncScheme;
	}
	
	public void setLocalSyncScheme(int localSyncScheme) {
		this.localSyncScheme = localSyncScheme;
	}
	
	public int getRemoteSyncScheme() {
		return remoteSyncScheme;
	}
	
	public void setRemoteSyncScheme(int remoteSyncScheme) {
		this.remoteSyncScheme = remoteSyncScheme;
	}
	
	public int getLocalSyncMode() {
		return localSyncMode;
	}
	
	public void setLocalSyncMode(int localSyncMode) {
		this.localSyncMode = localSyncMode;
	}
	
	public int getRemoteSyncMode() {
		return remoteSyncMode;
	}
	
	public void setRemoteSyncMode(int remoteSyncMode) {
		this.remoteSyncMode = remoteSyncMode;
	}

	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	public String getLastSync() {
		return lastSync;
	}

	public void setLastSync(String lastSync) {
		this.lastSync = lastSync;
	}

	public int getSyncOnWifi() {
		return wifiOnly;
	}

	public void setSyncOnWifi(int syncPreference) {
		this.wifiOnly = syncPreference;
	}
}